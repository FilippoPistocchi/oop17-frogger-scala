package controller.movement;

import model.frog.Frog;
import utilities.Constants;
import utilities.Direction;
/**
 * 
 */
public class MoveLeft implements Command {

    /**
     * Moves the frog based on the first key in the list. WASD used as directional keys.
     * @param frog the frog game object to move
     */

    public void execute(final Frog frog) {

        if (frog.getCenter() - frog.getLateralMovementValue() >= Constants.WORLD_LEFT_LIMIT 
                && frog.getCenter() - frog.getLateralMovementValue() <= Constants.WORLD_RIGHT_LIMIT) {
            frog.setFacing(Direction.FACING_LEFT);
            frog.moveTo(frog.getOccupiedLane(), frog.getCenter() - frog.getLateralMovementValue());
        }
    }
}
