package model;
 
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import controller.movement.Invoker;
import controller.movement.FrogCollision;
import javafx.scene.input.KeyCode;
import model.frog.Frog;
import model.gameobject.GameObject;
import model.gameobject.GameObjectImpl.GameObjectType;
import model.lane.LaneType;
import model.mod.ModObstacle;
import model.score.ScoreManager;
import model.score.ScoreManagerImpl;
import model.world.World;
import utilities.Constants;
import utilities.GameObserver;
/**
 *
 */
public class GameLogicImpl implements GameLogic {

    private static final int STARTING_LANE = 0;
    private static final int STARTING_LIVES = 3;

    /**
     * starting width, default is half lane.
     */
    public static final double STARTING_WIDTH = 50.0;
 
    private final Set<GameObserver> observers;
    private final Set<GameObserver> advanceObservers;
    private World world;
    private Frog frog;
    private final ScoreManager score;
    private GameDifficult diff;
    private final WorldCreator worldCreator;
    private final Invoker inputHandler;
    private int gameTime;


    /**
     * 
     */
    public GameLogicImpl() {
        inputHandler = new Invoker();
        score = new ScoreManagerImpl();
        worldCreator = new WorldCreatorImpl();
        observers = new HashSet<>();
        advanceObservers = new HashSet<>();
    }
 
    /**/
    @Override
    public void initializeGame(final int ups) {
        worldCreator.setUPS(ups);
        worldCreator.setDifficult(diff);
        world = worldCreator.createWorld();
        frog = new Frog(world, STARTING_LANE, 1.0, GameObjectType.FROG.create(STARTING_WIDTH));
        frog.setLives(STARTING_LIVES);
        gameTime = Constants.STARTING_GAME_TIME;
        score.resetScore();
    }
 
    /**/
    @Override
    public void update() {
        world.getLane().forEach(l -> l.update());
        frog.updateSpeed();
        frog.udpatePosition();
        checkCollision();
        //all den are filled, advance level
        if (world.getDen().stream().allMatch(d -> d.getGameObjectType() == GameObjectType.FILLED_DEN)) {
            System.out.println("Advance level");
            advanceLevel();
        }
    }

    /**/
    @Override
    public boolean gameOver() {
        return this.frog.getLives() <= 0 || this.gameTime <= 0;
    }
 
    private void checkCollision() {                                              //Same name as FrogCollision's method, could cause confusion

        final FrogCollision collision = new FrogCollision(frog, world);

        boolean endLaneHit = false;

        final Optional<GameObject> denHit = collision.denReached();

        if (denHit.isPresent()) {
            world.getDen().remove(denHit.get());
            //reset frog
            score.update(frog.getOccupiedLane());
            frog.moveTo(STARTING_LANE, Constants.WORLD_RIGHT_LIMIT / 2);
        } else { 
            if (world.getLane().get(frog.getOccupiedLane()).getLaneType() == LaneType.END_LANE  && !frog.isCheating()) {
                frog.setLives(frog.getLives() - 1);
                frog.moveTo(STARTING_LANE, Constants.WORLD_RIGHT_LIMIT / 2);
                score.frogDies();
                endLaneHit = true;
            }
        }

        if (collision.checkCollision() && !frog.isCheating()) {
            //remove life
            frog.setLives(frog.getLives() - 1);
            frog.moveTo(STARTING_LANE, Constants.WORLD_RIGHT_LIMIT / 2);
            //decrease score
            score.frogDies();
        } else if (!endLaneHit) {
            score.update(frog.getOccupiedLane());
        }

        final Optional<ModObstacle> mod = collision.modCollision();
        if (mod.isPresent()) {
            observers.forEach(o -> o.update(mod.get()));
        }
    }

    /**
     * invoke the received input.
     * @param input input received
     */
    @Override
    public void registerInput(final Optional<KeyCode> input) {
        if (input.isPresent()) {                                                //Check duplicated in move class, this can be made nullable but will result in more useless calls
            inputHandler.invoke(frog, input);
        }
    }

    /**
     * create a new world with increasing difficult.
     */
    private void advanceLevel() {
        advanceObservers.forEach(o -> o.update(null));
        if (diff.equals(GameDifficult.EASY)) {
            diff = GameDifficult.NORMAL;
        } else if (diff.equals(GameDifficult.NORMAL)) {
            diff = GameDifficult.HARD;
        }
        worldCreator.setDifficult(diff);
        this.world = worldCreator.createWorld();
        System.out.println(diff);
        frog = new Frog(world, STARTING_LANE, 1.0, GameObjectType.FROG.create(STARTING_WIDTH));
        gameTime = Constants.STARTING_GAME_TIME;
        frog.setLives(STARTING_LIVES);
        score.setDifficult(diff);
    }

    /**/
    @Override
    public void advanceTime(final int toDec) {
       gameTime -= toDec;
    }

    /**/
    @Override
    public int getTime() {
        return gameTime;
    }
 
    /**/
    @Override
    public void setDifficult(final GameDifficult d) {
        this.diff = d;
        score.setDifficult(diff);
    }
 
    /********************* GENERAL PURPOSE GETTERS ************************/
 
    /**
     * Default getter for the World.
     * @return world
     */
    public World getWorld() {
        return world;
    }
 
    /**
     * Default getter for the Frog.
     * @return frog
     */
    public Frog getFrog() {
        return frog;
    }
 
    /**
     * Default getter for the ScoreManager.
     * @return scoreManager
     */
    public ScoreManager getScoreManager() {
        return score;
    }
 
    /**
     * Default getter for the difficulty.
     * @return difficult
     */
    public GameDifficult getDifficult() {
        return diff;
    }
 
    /**
     * Default getter for the InputHandler.
     * @return InputHandler
     */
    public Invoker getInputHandler() {
        return inputHandler;
    }

    /**************** REGISTER OBSERVERS ****************/
    /**/
    @Override
    public void registerObserver(final GameObserver g) {
        this.observers.add(g);
    }
 
    /**/
    @Override
    public void detachObservers() {
        this.observers.clear();
        this.advanceObservers.clear();
    }

    /**/
    @Override
    public void registerAdvancedObserver(final GameObserver g) {
        this.advanceObservers.add(g);
    }
}
