package model.frog;

import model.gameobject.GameObject;
import model.gameobject.ObstacleBaseType;
import model.lane.LaneType;
import model.world.World;
import utilities.Constants;
import utilities.Direction;

/**
 * 
 */
public class Frog extends GameObjDecorator {

    private static final double LATERAL_MOVEMENT_VALUE = 7.0;

    private final World world;
    private Direction facing = Direction.FACING_UP;
    private boolean cheating;

    /**
     * The amount the frog will move horizontally every input.
     */
    private double lateralMovementValue = LATERAL_MOVEMENT_VALUE;
    /**
     * The amount the frog will move vertically every input.
     */
    private int verticalMovementValue = 1;

    /**
     * Set the amount the frog will move horizontally every input.
     * @param lateralMovementValue the value the frog will move horizontally every input
     */
    public void setLateralMovementValue(final double lateralMovementValue) {
        this.lateralMovementValue = lateralMovementValue;
    }

    /**
     * Set the amount the frog will move vertically every input, corresponds to the number of lane moved.
     * @param verticalMovementValue the value the frog will move vertically every input
     */
    public void setVerticalMovementValue(final int verticalMovementValue) {
        this.verticalMovementValue = verticalMovementValue;
    }

    /**
     * Gets the value of field lateralMovementValue.
     * @return lateralMovementValue
     */
    public double getLateralMovementValue() {
        return lateralMovementValue;
    }

    /**
     * Gets the value of field verticalMovementValue.
     * @return verticalMovementValue
     */
    public int getVerticalMovementValue() {
        return verticalMovementValue;
    }

    /**
     * @param world The world the frog is in
     * @param occupiedLane the lane the frog is in
     * @param speed the speed the frog has
     * @param gameObject the gameObjec use as a base to create the frog, the object MUST have FROG type
     */
    public Frog(final World world, final int occupiedLane, final double speed, final GameObject gameObject) {
        super(occupiedLane, speed, gameObject);
        if (occupiedLane < 0 || occupiedLane > Constants.WORLD_NUMBER_OF_LANE) {
            throw new IllegalArgumentException("Frog cannot be created on a non-exixting lane");
        }

        if (gameObject.getBaseType() != ObstacleBaseType.FROG) {
            throw new IllegalArgumentException("Frog cannot be created with a type different from FROG");
        }
        this.world = world;
    }

    /**
     * Set the current speed of the frog according to the lane it is in.
     */
    public final void updateSpeed() {
        if (world.getLane().get(getOccupiedLane()).getLaneType() == LaneType.RIVER) {
            super.setSpeed(world.getLane().get(getOccupiedLane()).getSpeed());
        } else {
            super.setSpeed(0.0);
        }
    }

    /**
     * Move to desired location.
     * @param lane The lane the frog should be moved to 
     * @param center The new center the frog should have
     */
    public final void moveTo(final int lane, final double center) {
        super.setOccupiedLane(lane);
        super.setCenter(center);
    }

    /**
     * Setter for the direction the frog is facing towards.
     * @param facing the direction the frog is facing.
     */
    public void setFacing(final Direction facing) {
        this.facing = facing;
    }

    /**
     * Getter for the direction the frog is facing towards.
     * @return The direction the frog is facing towards.
     */
    public Direction getFacing() {
        return facing;
    }

    /**
     * Get current status of cheating mode.
     * @return true if the frog is cheating, false otherwise. 
     */
    public boolean isCheating() {
        return this.cheating;
    }

    /**
     *  Toggles the cheating of the frog.
     */
    public void toggleCheating() {
        this.cheating = !this.cheating;
    }

}
