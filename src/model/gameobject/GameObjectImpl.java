package model.gameobject;

/**
 *
 */
public final class GameObjectImpl implements GameObject { // NOPMD

    /* 
     * Suppressed warning "Class cannot be instantiated and does not provide any static methods or fields"
     * This is not correct because there's a public constructor so class can be instantiated.
     */

    /**
     * GameObjectType represents what the object is.
     * The first parameter is the width of the obstacle.
     * The second parameter is the BaseType of the GameObject.
    */
    public enum GameObjectType {

        /**
         *
         */
        TRUCK(12.0, ObstacleBaseType.VEHICLE),

        /**
         *
         */
        PURPLE_CAR(7.0, ObstacleBaseType.VEHICLE),

        /**
         *
         */
        RACE_CAR(7.0, ObstacleBaseType.VEHICLE),

        /**
         * 
         */
        YELLOW_CAR(7.0, ObstacleBaseType.VEHICLE),

        /**
         *
         */
        WHITE_CAR(6.0, ObstacleBaseType.VEHICLE),

        /**
         *
         */
        BIG_LOG(40.0, ObstacleBaseType.LOG),

        /**
         *
         */
        MEDIUM_LOG(26.0, ObstacleBaseType.LOG),

        /**
         *
         */
        SMALL_LOG(19.0, ObstacleBaseType.LOG),

        /**
         * 
         */
        TURTLE(6.0, ObstacleBaseType.TURTLE),

        /**
         * 
         */
        MOD(8.0, ObstacleBaseType.MOD),

        /**
         * 
         */
        FROG(5.0, ObstacleBaseType.FROG),

        /**
         * 
         */
        FILLED_DEN(10.0, ObstacleBaseType.DEN),

        /**
         * 
         */
        EMPTY_DEN(10.0, ObstacleBaseType.DEN);

        private final double width;
        private final ObstacleBaseType baseType;

        GameObjectType(final double width, final ObstacleBaseType baseType) {
            this.width = width;
            this.baseType = baseType;
        }

        /**
         * @return the width of the obstacle.
         */
        public double getWidth() {
            return this.width;
        }

        /**
         *
         * @return the baseType effective type of the obstacle.
         */
        public ObstacleBaseType getBaseType() {
            return this.baseType;
        }

        /**
         * 
         * @param center the center of the obstacle to create.
         * @return the new obstacle.
         */
        public GameObject create(final double center) {
            return new GameObjectImpl(center, this);
        }

    }

    private double center;
    private final GameObjectType obstacleType;

    /**
     * 
     * @param center is the center of the obstacle.
     * @param obstacleType is the type of the obstacle.
     */
    private GameObjectImpl(final double center, final GameObjectType obstacleType) {
        this.center = center;
        this.obstacleType = obstacleType;
    }

    /**
     * 
     */
    @Override
    public double getCenter() {
        return this.center;
    }

    /**
     * 
     */
    @Override
    public void setCenter(final double newCenter) {
        this.center = newCenter;
    }

    /**
     * 
     */
    @Override
    public GameObjectType getGameObjectType() {
        return this.obstacleType;
    }

    /**
     * 
     */
    @Override
    public double getWidth() {
        return this.obstacleType.getWidth();
    }

    /**
     * 
     */
    @Override
    public ObstacleBaseType getBaseType() {
        return this.obstacleType.getBaseType();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        long temp;
        temp = Double.doubleToLongBits(center);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result + ((obstacleType == null) ? 0 : obstacleType.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GameObjectImpl other = (GameObjectImpl) obj;
        if (Double.doubleToLongBits(center) != Double.doubleToLongBits(other.center)) {
            return false;
        }
        return obstacleType == other.obstacleType;
    }
}
