package model.gameobject;

/**
 * This enumeration represents the base type of an obstacle.
 * Now is easier distinguish obstacles.
 *
 */
public enum ObstacleBaseType {

    /**
     * Logs moves in the river.
     */
    LOG,

    /**
     * Vehicles moves in the street.
     */
    VEHICLE,

    /**
     * Turtles moves in the river.
     */
    TURTLE,

    /**
     * Mod can be in each lane.
     */
    MOD,

    /**
     * The frog.
     */
    FROG,

    /**
     * A den.
     */
    DEN;

}
