package model.lane;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import model.gameobject.GameObject;
import model.gameobject.GameObjectImpl.GameObjectType;

/**
 * The last lane of the field.
 * End lane has dens.
 */
public class EndLane extends AbstractLane {

    private static final List<Double> DEN_CENTERS = new LinkedList<>(Arrays.asList(10.0, 30.0, 50.0, 70.0, 90.0));
    private final List<GameObject> dens;

    /**
     * 
     */
    public EndLane() {
        super(LaneType.END_LANE);
        this.dens = new LinkedList<>();
        DEN_CENTERS.forEach(c -> {
            this.dens.add(GameObjectType.FILLED_DEN.create(c));
            this.dens.add(GameObjectType.EMPTY_DEN.create(c));
        });
    }

    /**
     * 
     */
    @Override
    public void update() {

    }

    /**
     * This method doesn't return a defensive copy so the list can be manipulated.
     */
    @Override
    public List<GameObject> getObstacle() {
        return this.dens;
    }

    /**
     * 
     */
    @Override
    public double getSpeed() {
        return 0;
    }

    /**
     * 
     */
    @Override
    public void setSpeed(final double newSpeed) {

    }

}
