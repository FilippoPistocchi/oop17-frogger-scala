package model.score;

import model.GameDifficult;
import utilities.Constants;

/**
 * 
 */
public class ScoreManagerImpl implements ScoreManager {

    private static final String MISSING_DIFFICULT_EXCEPTION_MESSAGE = "Missing difficult.";

    private static final int POINT_PER_LANE = 5;
    private static final int POINT_PER_DEN = 50;
    private static final int POINT_PER_DEATH = -10;

    private int score;
    private int laneReached;
    private int scoreMultiplier;

    /**
     * 
     */
    public ScoreManagerImpl() {
        this.score = 0;
        this.laneReached = 0;
        this.scoreMultiplier = 0;
    }

    /**
     * 
     */
    @Override
    public void setDifficult(final GameDifficult difficult) {
        switch (difficult) {
        case EASY:
            this.scoreMultiplier = 1;
            break;
        case NORMAL:
            this.scoreMultiplier = 2;
            break;
        case HARD:
            this.scoreMultiplier = 3;
            break;
        default:
            break;
        }
    }

    /**
     * 
     */
    @Override
    public int getScore() {
        return this.score;
    }

    /**
     * 
     */
    @Override
    public void addScore(final int score) {
        this.missingDifficultException();
        this.score += score;
    }

    /**
     * 
     */
    @Override
    public void update(final int lane) {
        this.missingDifficultException();
        if (Constants.WORLD_NUMBER_OF_LANE - 1 == lane) {
            this.laneReached = 0;
            this.score += ScoreManagerImpl.POINT_PER_DEN;
        } else {
            if (laneReached < lane) {
                this.score += ScoreManagerImpl.POINT_PER_LANE * this.scoreMultiplier;
                this.laneReached++;
            }
        }
    }
    /**
     * 
     */
    public void resetScore() {
        this.score = 0;
    }

    /**
     * 
     */
    @Override
    public void frogDies() {
        this.missingDifficultException();
        this.score += ScoreManagerImpl.POINT_PER_DEATH * this.scoreMultiplier;
        if (score < 0) {
            score = 0;
        }
        this.laneReached = 0;
    }

    private void missingDifficultException() {
        if (this.scoreMultiplier == 0) {
            throw new UnsupportedOperationException(MISSING_DIFFICULT_EXCEPTION_MESSAGE);
        }
    }

}
