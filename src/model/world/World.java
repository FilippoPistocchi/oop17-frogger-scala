package model.world;

import java.util.List;

import model.gameobject.GameObject;
import model.lane.Lane;


/**
 * This interface represents the main game field.
 * It consists of lanes and dens.
 */
public interface World {

    /**
     * @return a list of lane.
     */
    List<Lane> getLane();

    /**
     * @return dens.
     */
    List<GameObject> getDen();

}
