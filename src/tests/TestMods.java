package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

import model.gameobject.GameObjectImpl.GameObjectType;
import model.mod.ModAlreadyPresentException;
import model.mod.ModEntity;
import model.mod.ModType;
import model.world.World;
import model.world.WorldImpl;

/**
 * JUnit tests for mods.
 */
public class TestMods {

    private static final String MESSAGE_SAME = "Should be the same";
    private static final double SPAWN_POSITION_1 = 0.80;
    private static final double SPAWN_POSITION_2 = 0.10;


    private static final World WORLD = new WorldImpl.Builder().addSafeLane()
            .addStreet(2, GameObjectType.PURPLE_CAR, 4)
            .addStreet(4, GameObjectType.RACE_CAR, 2)
            .addStreet(2, GameObjectType.WHITE_CAR, 3)
            .addStreet(2, GameObjectType.TRUCK, 3)
            .addStreet(2, GameObjectType.TRUCK, 3)
            .addSafeLane()
            .addRiver(3, GameObjectType.SMALL_LOG, 4)
            .addRiver(2, GameObjectType.MEDIUM_LOG, 2)
            .addRiver(4, GameObjectType.SMALL_LOG, 4)
            .addRiver(3, GameObjectType.BIG_LOG, 1)
            .addRiver(4, GameObjectType.SMALL_LOG, 4)
            .addEndLane()
            .build();
    /**
     * Test that controls whether it's possible to add more than one mod to the same lane.
     */
    @Test
    public void testAddingInSameLane() {
        try {
            WORLD.getLane().get(0).addMod(new ModEntity(GameObjectType.MOD.create(SPAWN_POSITION_1), ModType.ADDLIFE));
            WORLD.getLane().get(0).addMod(new ModEntity(GameObjectType.MOD.create(SPAWN_POSITION_2), ModType.FASTERSPEED));
            fail("Can't add two mods to the same lane");
        } catch (ModAlreadyPresentException e) {
            assertEquals(MESSAGE_SAME, WORLD.getLane().get(0).getMods().size(), 1);
            assertEquals(MESSAGE_SAME, WORLD.getLane().get(0).getMods().get(0).getModType(), ModType.ADDLIFE);
        }
    }

    /**
     * Test that controls whether it's possible to delete non-existing mods.
     */
    @Test (expected = IllegalStateException.class)
    public void testRemovingWhenNotPresent() {
        assertEquals(MESSAGE_SAME, WORLD.getLane().get(0).getMods().size(), 0);
        final ModEntity mod = new ModEntity(GameObjectType.MOD.create(0.10), ModType.ADDLIFE);
        WORLD.getLane().get(0).removeMod(mod);
        fail("Can't remove a non-existing mod");
    }
}
