package utilities;

import java.util.HashMap;
import java.util.Map;

import model.GameDifficult;
import model.gameobject.GameObjectImpl.GameObjectType;
import model.mod.ModType;

/**
 * This is an utility class containing public constants.
 */
public final class Constants {

    /**
     * Default difficult setting.
     */
    public static final GameDifficult DIFF_DEFAULT = GameDifficult.EASY;

    /**
     * Default fps setting.
     */
    public static final int FPS_DEFAULT = 60;

    /**
     * The left limit of the field.
     */
    public static final double WORLD_LEFT_LIMIT = 0.0;

    /**
     * The right limit of the field.
     */
    public static final double WORLD_RIGHT_LIMIT = 100.0;

    /**
     * The number of lanes in the world.
     */
    public static final int WORLD_NUMBER_OF_LANE = 13;

    /**
     * Number of roads in the world.
     */
    public static final int WORLD_NUMBER_OF_ROADS = 5;

    /**
     * Starting time.
     */
    public static final int STARTING_GAME_TIME = 120;

    /**
     * This is an array containing all the available ModTypes.
     */
    public static final ModType[] MODARRAY = ModType.values();

    /**
     * This is the map that links each GameObjectType to its texture's location.
     */
    public static final Map<GameObjectType, String> TEXTURES = new HashMap<>();

    private Constants() {

    }

}
