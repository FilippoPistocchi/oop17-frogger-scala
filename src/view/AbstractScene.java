package view;

import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.Parent;
import javafx.scene.Scene;
/**
 * template class for application Scene.
 */
public abstract class AbstractScene extends Scene {

    private static final int FONT_MULTI = 25;
    /**
     * font size property.
     */
    private final DoubleProperty fontSize;
    /**
     * contructor for scene.
     * @param root node of the scene
     * @param width of the scene
     * @param height of the scene
     */
    public AbstractScene(final Parent root, final double width, final double height) {
        super(root, width, height);
        this.getStylesheets().add((getClass().getResource("/style.css").toExternalForm()));
        fontSize = new SimpleDoubleProperty(this.widthProperty().divide(FONT_MULTI).get());
        root.styleProperty().bind(Bindings.format("-fx-font-size: %.2fpt;", fontSize));
    }
    /**
     * getter for the font size.
     * @return font size as double property
     */
    protected DoubleProperty getFontSize() {
        return fontSize;
    }

    /**
     * initialize scene data.
     */
    public abstract void initialize();

}
