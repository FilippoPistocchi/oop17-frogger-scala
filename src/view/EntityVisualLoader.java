package view;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import javafx.scene.image.Image;

/**********
 * The author of this class knows that the Singleton pattern is not thread safe, but
 * PMD was suppressed because thread safety is not an issue in this particular case.
 **********/

/**
 * A class that basically translates the type of entity to draw to the corresponding Image.
 * It contains a map <TranslationToFile, Image> that keeps already loaded images for faster access (like a cache).
 * EntityVisualLoader uses the Singleton pattern for initialization.
 */
public final class EntityVisualLoader {

    private static EntityVisualLoader visualLoader;
    private final Map<String, Image> cache;

    private EntityVisualLoader() {
        cache = new HashMap<>();
    }

    /**
     * Method to get the only instance of the EntityVisualLoader Singleton.
     * @return SINGLETON 
     */
    public static EntityVisualLoader getVisualLoader() {
        if (visualLoader == null) { //NOPMD
            visualLoader = new EntityVisualLoader();
        }
        return visualLoader;
    }

    /**
     * Method that simply gets the needed resources from files indicated by the string passed as an argument
     * and saves the resulting images to a cache. Obviously, if there are any problems
     * locating the files, it throws a FileNotFoundException.
     * @param s the String of the entity's texture to get
     * @return the selected Image
     * @throws FileNotFoundException 
     */
    public Image getResource(final String s) throws FileNotFoundException {
        /*
         *      If Image was cached already, the method takes 
         *      it from the internal map, so it's faster
         *      and doesn't need access to the filesystem.
         */ 
        if (cache.containsKey(s)) {
            return cache.get(s);
        }
        /*      Otherwise: Image is loaded from file.        */
        final Image img = new Image(this.getClass().getResource(s).toExternalForm());
        cache.put(s, img);
        return img;
    }

}
