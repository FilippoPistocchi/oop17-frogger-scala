package view;

import javafx.application.Platform;
import javafx.geometry.Rectangle2D;
import javafx.scene.input.KeyCode;
import javafx.scene.text.Font;
import javafx.stage.Screen;
import javafx.stage.Stage;
import model.gameobject.GameObjectImpl.GameObjectType;
import utilities.Constants;
import view.game.GameScene;
import view.game.ViewableEntity;
import view.menu.EndMenu;
import view.menu.LeaderboardMenu;
import view.menu.MainMenu;
import view.menu.OptionMenu;
import view.sound.SoundManager;
import view.sound.SoundManagerImpl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import controller.Controller;

/**
 * Main class of the app, it contains the main stage.
 */
public class ViewImpl implements View {
    /**
     * screens of the app.
     */
    public enum GameScreen {
        /**
         * game scene.
         */
        GAME,
        /**
         * main menu scene.
         */
        MAINMENU,
        /**
         * options scene.
         */
        OPTIONS,
        /**
         * leaderboard scene.
         */
        LEADERBOARD,
        /**
         * end menu scene.
         */
        ENDMENU
    }

    private static Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
    private static final double HEIGHT_MULTIPLIER = 1.2;
    private static final double WIDTH_MULTIPLIER = 3;
    private static final double MINIMUM_MULTIPLIER = 0.90;

    /**
     * This constant boolean is the default value for the option that
     * sets the view resizable.
     */
    public static final boolean RES_DEFAULT = false;

    private final Stage mainStage;
    private AbstractScene currentScene;
    private final Map<GameScreen, AbstractScene> scenes;
    private Controller controller;
    private final List<KeyCode> inputs;
    private boolean resizable = RES_DEFAULT;
    private SoundManager sounds;
    /**
     * constructor.
     * @param s main stage
     */
    public ViewImpl(final Stage s) {
        mainStage = s;
        scenes = new HashMap<>();
        scenes.put(GameScreen.GAME, new GameScene(this.getExecutionWidth(), this.getExecutionHeight(), this));
        scenes.put(GameScreen.MAINMENU, new MainMenu(this.getExecutionWidth(), this.getExecutionHeight(), this));
        scenes.put(GameScreen.OPTIONS, new OptionMenu(this.getExecutionWidth(), this.getExecutionHeight(), this));
        scenes.put(GameScreen.LEADERBOARD, new LeaderboardMenu(this.getExecutionWidth(), this.getExecutionHeight(), this));
        scenes.put(GameScreen.ENDMENU, new EndMenu(this.getExecutionWidth(), this.getExecutionHeight(), this));

        /*****INITIALIZATION OF TEXTURE MAP******/
        final String base = "/img/";
        Constants.TEXTURES.put(GameObjectType.FROG, base + "frog/frog.png");
        Constants.TEXTURES.put(GameObjectType.PURPLE_CAR, base + "obstacles/car/car_purple.png");
        Constants.TEXTURES.put(GameObjectType.RACE_CAR, base + "obstacles/car/car_race.png");
        Constants.TEXTURES.put(GameObjectType.TRUCK, base + "obstacles/car/truck.png");
        Constants.TEXTURES.put(GameObjectType.WHITE_CAR, base + "obstacles/car/car_white.png");
        Constants.TEXTURES.put(GameObjectType.YELLOW_CAR, base + "obstacles/car/car_yellow.png");
        Constants.TEXTURES.put(GameObjectType.BIG_LOG, base + "obstacles/log/log_l.png");
        Constants.TEXTURES.put(GameObjectType.MEDIUM_LOG, base + "obstacles/log/log_m.png");
        Constants.TEXTURES.put(GameObjectType.SMALL_LOG, base + "obstacles/log/log_s.png");
        Constants.TEXTURES.put(GameObjectType.TURTLE, base + "turtle/turtle.png");
        Constants.TEXTURES.put(GameObjectType.MOD, base + "mod.png");
        Constants.TEXTURES.put(GameObjectType.EMPTY_DEN, base + "textures/den.png");
        Constants.TEXTURES.put(GameObjectType.FILLED_DEN, base + "textures/occupied_den.png");

        /***** SOUND MANAGER INITIALIZATION *****/
        this.sounds = new SoundManagerImpl(this);

        /***** INPUTS INITIALIZATION *****/
        inputs = new LinkedList<>();

        /***** FONTS INITIALIZATION *****/
        Font.loadFont(ViewImpl.class.getResource("/fonts/ARCADECLASSIC.TTF").toExternalForm(), 10);
    }
    /**
     * start the view and showing main menu.
     */
    public void startMenu() {
        currentScene = scenes.get(GameScreen.MAINMENU);
        mainStage.setScene(currentScene);
        mainStage.centerOnScreen();
        mainStage.setTitle("JavaFX - Frogger");
        mainStage.setResizable(resizable);
        mainStage.setOnCloseRequest(e -> Platform.exit());
        mainStage.setMinHeight(getExecutionHeight());
        mainStage.setMinWidth(getExecutionWidth());
        sounds.play(GameScreen.MAINMENU);
        mainStage.show();
    }
    /**
     * change the scene to be displayed on mainStage.
     * @param s GameScreen selection.
     * @return currentScene displayed scene
     */
    public AbstractScene changeScene(final GameScreen s) {
        final double height = mainStage.getHeight();
        final double width = mainStage.getWidth();
        if (scenes.containsKey(s)) {
            currentScene = scenes.get(s);
            if (s.equals(GameScreen.ENDMENU)) {
                sounds.end();
                sounds = new SoundManagerImpl(this);
            }
            sounds.play(s);
            Platform.runLater(() -> {
                mainStage.setScene(currentScene);
                mainStage.setHeight(height);
                mainStage.setWidth(width);
            });
            mainStage.setResizable(resizable);
        } else {
            throw new IllegalArgumentException();
        }
        return currentScene;
    }

    /**
     * set controller reference.
     * @param c controller
     */
    public void setController(final Controller c) {
        this.controller = c;
    }
    /**
     * getter for controller.
     * @return controller
     */
    public Controller getController() {
        return this.controller;
    }

    /**
     * 
     * @param l The List of ViewableEntities to draw;
     */
    public void draw(final List<ViewableEntity> l) {
        final GameScene s = getGameScene();
        Platform.runLater(() -> s.drawEntities(l));
    }

    /**
     * @param dens The list of dens positions and isFree fields.
     */
    public void drawDens(final List<ViewableEntity> dens) {
        final GameScene s = getGameScene();
        Platform.runLater(() -> s.drawDens(dens));
    }

    /**
     * @param newTimePercentage The remaining time in a double between 0 and 1 (a percentage).
     */
    @Override
    public void updateTimer(final double newTimePercentage) {
        final GameScene s = getGameScene();
        Platform.runLater(() -> s.updateTimer(newTimePercentage));
    }

    private GameScene getGameScene() {
        return (GameScene) scenes.get(GameScreen.GAME);
    }

    /**
     * Calculates the execution height depending on
     * the screen resolution.
     * @return The height of the game.
     */
    private double getExecutionHeight() {
        return screenBounds.getHeight() / HEIGHT_MULTIPLIER / MINIMUM_MULTIPLIER;
    }

    /**
     * Calculates the execution width depending on
     * the screen resolution.
     * @return The width of the game.
     */
    private double getExecutionWidth() {
        return screenBounds.getWidth() / WIDTH_MULTIPLIER / MINIMUM_MULTIPLIER;
    }

    /**/
    @Override
    public Optional<KeyCode> getInput() {
        final Optional<KeyCode> first = inputs.isEmpty() ? Optional.empty() : Optional.of(inputs.get(0));
        inputs.clear();
        return first;
    }

    /**/
    @Override
    public void addInput(final KeyCode k) {
        this.inputs.add(k);
    }

    /**/
    @Override
    public void updateScore(final int newScore) {
        Platform.runLater(() -> getGameScene().updateScore(newScore));
    }

    /**/
    @Override
    public void updateLives(final int newLives) {
        Platform.runLater(() -> getGameScene().updateLives(newLives));
    }

    /**/
    @Override
    public void setModText(final String s) {
        Platform.runLater(() -> getGameScene().updateModView(s));
    }

    /**/
    @Override
    public void clearModText() {
        Platform.runLater(() -> getGameScene().clearModView());
    }

    /**/
    @Override
    public void setResizable(final boolean resizable) {
        this.resizable = resizable;
    }
}
