package view.game;

import view.BackgroundTexture;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.IntStream;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import model.lane.LaneType;
import utilities.Constants;
import view.AbstractGameScene;
import view.EntityVisualLoader;
import view.View;
import javafx.scene.shape.Rectangle;

/**
 * This is the main game window.
 */
public class GameScene extends AbstractGameScene {

//Static constants
    private static final AnchorPane OUTER = new AnchorPane();
    private static final double CORNER_OFFSET = 3.0;
    private static final double LIVES_OFFSET = 5.0;
    private static final double LIVES_DIMENSION = 20.0;
    private static final double BUTTONS_WIDTH_MULTIPLIER = 50.0;
    private static final double BUTTONS_HEIGHT_MULTIPLIER = 55.0;
    private static final double INSETS_TOP = 15;
    private static final double BASE_INSET = 25;
    private static final double RATIO = 10;
    private static final String LOADING_ERROR = "Errore di caricamento immagine: ";
//Global variables
    private ImageView bckgImg; //NOPMD
    private final ProgressBar timer;
    private final List<LaneView> lanes;
    private final GridPane stateGrid; //NOPMD
    private final GridPane viewStack;
    private final GridPane timeGrid; //NOPMD
    private final GridPane scoreGrid; //NOPMD
    private final GridPane lives; //NOPMD
    private final BorderPane textPane; //NOPMD
//View elements to be changed dynamically
    private final Label scoreLabel;
    private final HBox livingRepr;
    private final Label newModActivated;
//Default constants
    private static final int LANE_NUM = 13;

    /**
     * Constructor for the GameView.
     * @throws Exception when building the lanes and textures are not found.
     * @param height height of the scene
     * @param width width of the scene
     * @param v The view this GameScene is connected to.
     */
    //@SuppressWarnings("static-access")
    public GameScene(final double width, final double height, final View v) {
/*      The AnchorPane which controls the outer components 
 *      of the window is referred to as "OUTER"
 */
        super(OUTER, width, height);

        /*TOP*/

        //"pause" and "run" buttons must be shown in the top-left corner of the window
        stateGrid = new GridPane();
        final ToggleGroup state = new ToggleGroup();
        final ToggleButton pause = new ToggleButton();
        final ToggleButton run = new ToggleButton();
        try {
            final ImageView pauseImg = getImageView(getImage("/ico/pause.png"));
            final ImageView startImg = getImageView(getImage("/ico/start.png"));
            pauseImg.setFitHeight(height / BUTTONS_HEIGHT_MULTIPLIER);
            pauseImg.setFitWidth(width / BUTTONS_WIDTH_MULTIPLIER);
            startImg.setFitHeight(height / BUTTONS_HEIGHT_MULTIPLIER);
            startImg.setFitWidth(width / BUTTONS_WIDTH_MULTIPLIER);
            pause.setGraphic(pauseImg);
            run.setGraphic(startImg);
        } catch (FileNotFoundException e) {
            System.err.println("File not found.");
        }
        pause.setToggleGroup(state);
        run.setToggleGroup(state);
        stateGrid.add(pause, 0, 0);
        stateGrid.add(run, 1, 0);
        state.selectToggle(run);
        pause.setOnAction(e -> v.getController().pauseGame());
        run.setOnAction(e -> v.getController().resumeGame());

        //A Grid that must be shown in the top-right corner of the window
        timeGrid = new GridPane();
        timeGrid.setHgap(10);
        //ProgressBar will show the correct remaining time
        timer = new ProgressBar();
        timer.setPrefWidth(width / 4);
        final Text timerText = new Text("Timer");
        timeGrid.add(timerText, 0, 0);
        timeGrid.add(timer, 1, 0);

        //A label that must be shown on the center-top of the view in a BorderPane
        this.textPane = new BorderPane();
        final VBox labels = new VBox();
        newModActivated = new Label();
        labels.getChildren().add(newModActivated);
        newModActivated.setId("new-mod-alert");

        labels.setAlignment(Pos.CENTER);
        textPane.setCenter(labels);

        /*MID*/

        //A GridPane is used to keep all lanes.
        viewStack = new GridPane();
        lanes = new ArrayList<>();
        //A background texture for the world
        bckgImg = new ImageView();
        try {
            bckgImg = getImageView(getImage(BackgroundTexture.BACKGROUND.getUrl()));
        } catch (Exception e) {
            System.err.println("Could not load background image");
        }
        bckgImg.setVisible(false);

        /*BOTTOM*/

/*      The following are the elements in the bottom corners of the window.
 *       The first component is a GridPane which shows the current score.
 *       This will be updated every time the frog progresses forward 
 *       or does something that involves score changes.
 *       This component will be anchored in the bottom-left corner.
 */
        scoreGrid = new GridPane();
        scoreGrid.setAlignment(Pos.BOTTOM_LEFT);
        scoreGrid.setHgap(10);
        scoreLabel = new Label(Integer.toString(0));
        final Text scoreText = new Text("Score");
        scoreGrid.add(scoreText, 0, 0);
        scoreGrid.add(scoreLabel, 1, 0);

/*      The second component is another GridPane showing 
 *      the current remaining lives.
 *      This will be updated when a life is lost or gained.
 *      This component will be anchored in the bottom-right corner.
 */
        lives = new GridPane();
        lives.setHgap(LIVES_OFFSET);
        lives.setAlignment(Pos.BOTTOM_RIGHT);
        livingRepr = new HBox(10);

        final Text livesText = new Text("Lives");
        lives.add(livesText, 0, 0);
        lives.add(livingRepr, 1, 0);

        /*GENERAL*/

        //Defining the properties of the outer AnchorPane
        OUTER.setPadding(new Insets(INSETS_TOP, BASE_INSET, BASE_INSET, BASE_INSET));
        //The stateGrid is anchored on the top left with a static offset
        AnchorPane.setTopAnchor(stateGrid, CORNER_OFFSET);
        AnchorPane.setLeftAnchor(stateGrid, CORNER_OFFSET);
        //The timeGrid is anchored on the top right with a static offset
        AnchorPane.setTopAnchor(timeGrid, CORNER_OFFSET);
        AnchorPane.setRightAnchor(timeGrid, CORNER_OFFSET);
        //The score Grid is anchored on the bottom-left corner with the same static offset
        AnchorPane.setBottomAnchor(scoreGrid, CORNER_OFFSET);
        AnchorPane.setLeftAnchor(scoreGrid, CORNER_OFFSET);
        //The lives Grid is anchored on the bottom-right corner with the same static offset
        AnchorPane.setBottomAnchor(lives, CORNER_OFFSET);
        AnchorPane.setRightAnchor(lives, CORNER_OFFSET);

/*      The ListView that contains the lanes in the game is anchored 
 *      to both the left and the right of the window, but also to the top 
 *      with a sceneHeight/10 offset and to the bottom with the same offset. 
 *      1/10 of the window for each "border" is a fair proportion, 
 *      but it might be changed easily
 */
        AnchorPane.setLeftAnchor(viewStack, CORNER_OFFSET);
        AnchorPane.setRightAnchor(viewStack, CORNER_OFFSET);
        AnchorPane.setTopAnchor(viewStack, height / RATIO);
        AnchorPane.setBottomAnchor(viewStack, height / RATIO);
        AnchorPane.setLeftAnchor(bckgImg, CORNER_OFFSET);
        AnchorPane.setRightAnchor(bckgImg, CORNER_OFFSET);
        AnchorPane.setTopAnchor(bckgImg, height / RATIO);
        AnchorPane.setBottomAnchor(bckgImg, height / RATIO);

/*      Same goes for the textPane which uses slightly different constants
 *      and no bottom anchors.
 */
        AnchorPane.setLeftAnchor(textPane, CORNER_OFFSET);
        AnchorPane.setRightAnchor(textPane, CORNER_OFFSET);
        AnchorPane.setTopAnchor(textPane, height / (RATIO * 2));

        //All corner elements are added to the outer AnchorPane
        OUTER.getChildren().add(stateGrid);
        OUTER.getChildren().add(timeGrid);
        OUTER.getChildren().add(textPane);
        OUTER.getChildren().add(scoreGrid);
        OUTER.getChildren().add(lives);
        OUTER.getChildren().add(bckgImg);
        OUTER.getChildren().add(viewStack);

        //Setting CSS properties to text
        timerText.setId("gameText");
        livesText.setId("gameText");
        scoreText.setId("gameText");

        //Adding an event handler for each pressed key
        this.addEventHandler(KeyEvent.KEY_PRESSED, (key) -> v.addInput(key.getCode()));
    }

    /************************************ DRAWING METHODS ***********************************/

    /**
     * This method receives a collection of all elements to draw and calls the drawEntites() method
     * in each lane with the correct parameters.
     * @param toDraw collection of ViewableEntities to be drawn.
     */
    public void drawEntities(final Collection<ViewableEntity> toDraw) {
        lanes.subList(1, lanes.size()).forEach(l -> l.prepare());
        toDraw.forEach(e -> draw(e));
    }

    /**
     * This method draws dens on the last lane. 
     * @param dens A collection of dens. This method is separate from the normal drawEntities
     * because it could be called only once every while in different implementations of
     * the GameScene.
     */
    public void drawDens(final Collection<ViewableEntity> dens) {
        lanes.get(0).prepare();
        dens.forEach(d -> draw(d));
    }

    private void draw(final ViewableEntity entity) {
        final LaneView lane = lanes.get(lanes.size() - 1 - entity.getLaneNum());
        final String texture = Constants.TEXTURES.get(entity.getType());
        try {
            final Image img = getImage(texture);
            lane.drawEntity(img, entity.getPosition() - (entity.getType().getWidth() / 2), 
                    entity.getType().getWidth(), entity.getDir().getAngle());
        } catch (FileNotFoundException e) {
            printTextureError(texture);
        }
    }

    /********************************* UPDATE METHODS *************************************/

    /**
     * Updates the score.
     * @param newScore the updated score.
     */
    public void updateScore(final Integer newScore) {
        this.scoreLabel.setText(Integer.toString(newScore));
    }

    /**
     * Updates the number of lives.
     * @param livesNum the number of lives to be shown.
     */
    public void updateLives(final Integer livesNum) {
      //#Currently using rectangles to represent lives. Might be modified in the future.
        livingRepr.getChildren().clear();
        IntStream.range(0, livesNum).mapToObj(i -> new Rectangle(LIVES_DIMENSION, LIVES_DIMENSION, Color.DARKGREEN))
                                   .forEach(b -> livingRepr.getChildren().add(b));
    }

    /**
     * Updates the timer.
     * @param newValue The value to update the timer with.
     */
    public void updateTimer(final double newValue) {
        this.timer.setProgress(newValue);
    }

    /**
     * Sets the text for the mod label that shows which mod has been activated.
     * @param s The string indicating the name of the ModType.
     */
    public void updateModView(final String s) {
        this.newModActivated.setText("New mod activated: " + s);
    }

    /**
     * Removes the text for the mod label (to be called after a while).
     */
    public void clearModView() {
        this.newModActivated.setText("");
    }

    /********************************** INIT METHODS *********************************/

    /**/
    @Override
    public void initialize(final List<LaneType> lanes) {
        defaultInitialization();

        /******** INITIALIZING LANES ********/
        for (int i = 0; i < lanes.size(); i++) {
            switch (lanes.get(lanes.size() - i - 1)) {
                case SAFE_LANE:
                    if (lanes.size() - i + 1 < lanes.size()) {
                        if (lanes.get(lanes.size() - i + 1).equals(LaneType.STREET)) {
                            addLane(BackgroundTexture.LANE_FIRST, viewStack, i);
                        } else {
                            addLane(BackgroundTexture.LANE_MID, viewStack, i);
                        }
                    } else {
                        addLane(BackgroundTexture.LANE_MID, viewStack, i);
                    }
                    break;
                case RIVER: addLane(BackgroundTexture.WATER, viewStack, i);
                    break;
                case STREET: addLane(BackgroundTexture.STREET, viewStack, i);
                    break;
                case END_LANE: addLane(BackgroundTexture.LANE_END, viewStack, i);
                    break;
                default: break;
            }
        }
    }

    private void defaultInitialization() {

        /******** CLEARING VIEW LEFTOVERS ********/
        viewStack.getChildren().clear();
        lanes.clear();
        updateLives(3);
        updateScore(0);
        timer.setProgress(1);

        /********** Setting BCKIMG **************/
        bckgImg.fitHeightProperty().bind(viewStack.heightProperty());
        bckgImg.fitWidthProperty().bind(viewStack.widthProperty());
        bckgImg.setVisible(true);

        /***** EntityVisualLoader CACHING *******/
        Constants.TEXTURES.entrySet().forEach(e -> {
            try {
                EntityVisualLoader.getVisualLoader().getResource(e.getValue());
            } catch (FileNotFoundException e1) {
                printTextureError(e.getValue());
            }
        });
    }

    /**************************** PRIVATE METHODS **********************************/

    /**
     * Adds a lane to the "lanes" list.
     * @param t The TextureLocation to be rendered on the lane (background image)
     * @param p The pane that contains the lane, so that we can use its height and width.
     */
    private void addLane(final BackgroundTexture t, final GridPane p, final int index) {
        try {
            final LaneView l = new LaneViewImpl(getImageView(getImage(t.getUrl())),
                                        p.heightProperty().divide(LANE_NUM),
                                        p.widthProperty());
            lanes.add(l);
            p.addRow(index, (LaneViewImpl) l);
        } catch (FileNotFoundException e) {
            System.err.print(LOADING_ERROR + t.getUrl());
        }
    }

    private Image getImage(final String s) throws FileNotFoundException {
        return EntityVisualLoader.getVisualLoader().getResource(s);
    }

    private ImageView getImageView(final Image img) {
        return new ImageView(img);
    }

    private void printTextureError(final String texture) {
        System.err.print(LOADING_ERROR + texture);
    }
}
