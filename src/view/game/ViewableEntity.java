package view.game;

import model.gameobject.GameObjectImpl.GameObjectType;
import utilities.Direction;

/**
 * This class makes it easier and more readable to send information from the controller 
 * to the main Game View.
 */
public class ViewableEntity {

    private final GameObjectType type;
    private double position;
    private final int laneNum;
    private final Direction dir;

    /**
     * Simple constructor for an EntityView element.
     * @param type The type of the GameObject to draw.
     * @param position The position of the entity center in its lane.
     * @param laneNum The number of the lane this entity is currently on.
     * @param dir The direction this entity is facing.
     */
    public ViewableEntity(final GameObjectType type, final double position, final int laneNum,
                            final Direction dir) {
        this.type = type;
        this.position = position;
        this.laneNum = laneNum;
        this.dir = dir;
    }

    /**
     * Getter for the position of the entity.
     * @return position
     */
    public double getPosition() {
        return position;
    }

    /**
     * Setter for the position of the entity.
     * @param position The new position of this entity.
     */
    public void setPosition(final double position) {
        this.position = position;
    }

    /**
     * Getter for the lane number of this entity.
     * @return laneNum
     */
    public int getLaneNum() {
        return laneNum;
    }

    /**
     * Getter for the direction this entity is facing.
     * @return direction
     */
    public Direction getDir() {
        return this.dir;
    }

    /**
     * Getter for the type of this entity.
     * @return type The GameObjectType.
     */
    public GameObjectType getType() {
        return type;
    }

}
